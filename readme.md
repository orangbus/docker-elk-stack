# elk服务

> elasticsearch:8.2.3
> 
> kibana::8.2.3
> 
> ik:8.2.3

别名设置
```text
echo "alias dc='docker-compose'" >> ~/.bashrc
echo "alias dcd='dc down'" >> ~/.bashrc
echo "alias dcps='dc ps'" >> ~/.bashrc
source ~/.bashrc
```
复制一份配置文件
```bash
cp .env.example .env
```
Ps: 以上步骤必须配置，然后再往下看（当然大佬可以跳过）。

启动所有服务
```bash
dc up
```
查看服务状态
```bash
dc ps
```

> elasticsearch
```bash
dc up -d elasticsearch
```
如果启动失败，可查看下对应的目录是否有权限，比如
```bash
sudo chmod -R 777 data
```
设置账号密码
```bash
dc exec elasticsearch sh

# 自动生成（须记住密码！！）
/usr/share/elasticsearch/bin/elasticsearch-setup-passwords auto
# 手动生成
/usr/share/elasticsearch/bin/elasticsearch-setup-passwords interactive
```
密码
```text
Changed password for user apm_system
PASSWORD apm_system = fdLAWyRhjCsxsTlcE0ik

Changed password for user kibana_system
PASSWORD kibana_system = NjhUSH9VMbpxfxaVm78V

Changed password for user kibana
PASSWORD kibana = NjhUSH9VMbpxfxaVm78V

Changed password for user logstash_system
PASSWORD logstash_system = 6pI4WpLFQFIKwBKOMlDo

Changed password for user beats_system
PASSWORD beats_system = F4DWMv5H0jwpmMOQCTIq

Changed password for user remote_monitoring_user
PASSWORD remote_monitoring_user = vGhz6xwI6HbVo15KVJ1P

Changed password for user elastic
PASSWORD elastic = 93Wzawo0wunszVmc1gwa
```

> kibana
```bash
dc up -d kibana
```
如果elasticsearch 配置了密码的话
```yaml
# vim kibanakibana.yml
elasticsearch.username: "kibana_system"
elasticsearch.password: "pass"
```
> filebeat
```bash
dc up -d filebeat
```
> logstash
```bash
dc up -d logstash
```
> head
```bash
dc up -d head
```

ik下载地址: https://github.com/medcl/elasticsearch-analysis-ik

# 踩坑指南
如果是第一次启动kibaba，他会提示你进行初始化配置，我们可以选择【手动配置】，对于【当前项目来说】连接elasticsearch的地址是：`http://elasticsearch:9200` ,因为`elasticsearch`和`kibana`通过link关联，即使你的`elasticsearch`暴露的端口不是 9200,也需要这么填。

下一步需要一个kibana的6位数的验证码，可以通过
```bash
dc log -f kibana
```
进行查看

其他暂时没啥了，如果你遇到新的问题，欢迎进群讨论：[511300462](https://jq.qq.com/?_wv=1027&k=t0YIjFgp)

